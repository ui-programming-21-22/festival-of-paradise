Festival of Paradise

=========================Sorting of Files=========================

JS Files
- loader.js -> Root. called by game.html to load all other scripts with JQuery!
- storage.js -> Assets/JS. Called by index.html to store data, like name,age, animal etc.
- main.js -> main game functions, such as pause, update, game input etc.
- scenes.js -> all the scene checks/ draws them.
- player.js -> player functions & pet functions.
- baker.js -> NPC 1 functions
^ all NPCs will have their own js


CSS Files
- stylesheet.css -> style the two html pages, responsitvity finally works (the text is still not centered, you can bite me)


HTML Files
- index.html -> start screen, loads the variables and storage by itself
- game.html -> game screen, loads the loader to minimalise the workload.



=========================Game Idea=========================

Character
The main character is just your average guy. (Player name)​
The player is followed by a pet of their choice. (To make it less lonely)​
The game is a side-scroller, with many cutscenes and two fun minigames!​
It is all pixel-art!​


Story
A disaster struck you town recently, but despite that, it is time for the annual festival. 
You were mailed to help out with a few tasks around, 
as your family usually would always organise the best festivals.​
You decide to head out with your helper (cat or dog) out into the town.

You have to get five things done for the party.​​
- [ ]     	-Get some cake/pastries ordered from the baker​​ (Tutorial)
- [ ]     	-Set up music with the local musician​​
- [ ]     	-Ask the winemaker to make the drinks​​
- [ ]     	-Get the table/chairs out (blacksmith has them in their storage)​​
- [ ]     	-See the damage from the disaster, and help out if needed (museum)​​

Was this game just an excuse to make many side quests? Perhaps!

Goal
Help around your hometown to set up for a festival as much as possible!​
Depending on how you do, your ending will change! Try to get the best one possible!
You have all the time you need to complete the set up, going home and going to bed ends the game.​​​
The party takes place the next day.




=========================Links=========================

Video Prototype: https://youtu.be/bz84U7Aohew​

All assets were drawn by me <3 

=========================Known Bugs=========================

-Modal is null in game.html, this is fine, it doesn't impact the game in any way.
-Sometimes you spawn in the middle of the house rather than the bed
-Sometimes the player sprite in teh forst doesnt load right, but it fixes itself after some movement

-we hit commit number 69... nice 😳