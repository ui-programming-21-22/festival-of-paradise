//This is just the web storage
console.log("Storage Loaded");


//local storage
if(typeof(Storage) !== "undefined") {
    // console.log("Local storage is supported.");
    // Local storage is available on your browser
    //web storage
    let username = localStorage.getItem('username'); 
    let age = localStorage.getItem('age');
    let animal = localStorage.getItem('animal');
    if (username)
    { 

        let form = document.getElementById("info");
        let modal = document.getElementById("checker");
        let modalContent = modal.children[0].children[2];
        let validateButton = document.getElementsByClassName("saved-data-accept")[0];
        let dismissButton = document.getElementsByClassName("saved-data-refusal")[0];
        form.style.display = "none";
        modal.style.display = "block";
        modalContent.innerHTML = "Name: " + username + "<br>" + "Age: " + age + "<br>" + "Animal: " + animal;
        validateButton.onclick = function()
        {
            modal.style.display = "none";
            form.style.display = "none";
            window.location.href = "game.html";
        }
        dismissButton.onclick = function()
        {
            modal.style.display = "none";
            form.style.display = "block";
            localStorage.clear();
        }
    }
    else
    {
        console.log("no data in localStorage, loading new session")
    }
  } 
  else 
  {
    console.log("Local storage is not supported.");
    // The condition isn't met, meaning local storage isn't supported
  }

// Stores the item data
function validateForm(){
    event.preventDefault();
    var x = document.forms["info"]["name"].value;
    var y = document.forms["info"]["age"].value;
    var z = document.forms["info"]["animal"].value;
    if (x == "") {
        alert("I need to know your name so I can say Hello");
        return false;
    }
    else{
        alert("Hello there " + document.forms["info"]["name"].value);
        window.location.href = "game.html";
        //more advanced pt2: make a system that changes the webpage based on the inputted name 
    }
    localStorage.setItem("username", x);
    localStorage.setItem("age", y);
    localStorage.setItem("animal", z);
}