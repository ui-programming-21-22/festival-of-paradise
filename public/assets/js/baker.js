//the baker quest and dialogue is stored here 
console.log("baker loaded");

function bakerQuestLine()
{
    //before talking
    if (bakerQ == 0)
    {
        bakerQ = 1;
        diax= 300;
    }   
    //during talking
    if (bakerQ == 1)
    {
        switch(action)
        {
            case 1:  
                dialogue.src = "assets/media/dialogue/baker1.png";
            break;
            case 2:  
                dialogue.src = "assets/media/dialogue/baker2.png";
            break;
            case 3:  
                dialogue.src = "assets/media/dialogue/baker3.png";
            break;
            case 4:  
                dialogue.src = "assets/media/dialogue/baker4.png";
            break;
            case 5:  
                dialogue.src = "assets/media/dialogue/baker5.png";
            break;
            case 6:  
                dialogue.src = "assets/media/dialogue/baker6.png";
            break;
        }
    } 
    //adventure so nothing lol
    if (bakerQ == 2)
    {
        
    } 

    //farmer talkinng
    if (bakerQ == 3)
    {
        diax= 300;

        switch(action)
        {
            case 7:  
                dialogue.src = "assets/media/dialogue/farmer1.png";
            break;
            case 8:  
                dialogue.src = "assets/media/dialogue/farmer2.png";
            break;
            case 9:  
                dialogue.src = "assets/media/dialogue/farmer3.png";
            break;
            case 10:  
                dialogue.src = "assets/media/dialogue/farmer4.png";
            break;
        }
        
    } 

}

function bakerQuestStart()
{
    if (bakerQ == 1)
    {
        switch(action)
        {
            case 1:  
                if (gamerInput.action === "Space")
                { 
                    setTimeout(function() { 
                        action = 2; 
                    }, 1500);
                }
            break;

            case 2:  
                if (gamerInput.action === "Space")
                { 
                    setTimeout(function() { 
                        action = 3; 
                    }, 1500);
                }
            break;

            case 3:  
                if (gamerInput.action === "Space")
                { 
                    setTimeout(function() { 
                        action = 4; 
                    }, 1500);
                }
            break;

            case 4:  
            if (gamerInput.action === "Space")
            { 
                setTimeout(function() { 
                    action = 5; 
                }, 1500);
            }
            break;

            case 5:  
            if (gamerInput.action === "Space")
            { 
                setTimeout(function() { 
                    action = 6; 
                }, 1500);
            }
            break;

            case 6:  
            if (gamerInput.action === "Space")
            { 
                setTimeout(function() { 
                    action = 7; 
                    diax = -1000;
                    bakerQ = 2;
                }, 1500);
            }
            break;
        }
    }

    if(bakerQ == 3)
    {
        switch(action)
        {
            case 7:  
                if (gamerInput.action === "Space")
                { 
                    setTimeout(function() { 
                        action = 8; 
                    }, 1500);
                }
            break;

            case 8:  
                if (gamerInput.action === "Space")
                { 
                    setTimeout(function() { 
                        action = 9; 
                    }, 1500);
                }
            break;

            case 9:  
                if (gamerInput.action === "Space")
                { 
                    setTimeout(function() { 
                        action = 10; 
                    }, 1500);
                }
            break;

            case 10:  
            if (gamerInput.action === "Space")
            { 
                setTimeout(function() { 
                    action = 11; 
                    bakerQ = 4;
                    diax = -1000;
                }, 1500);
            }
        break;
        }
    }
}

