//All player based functions are here, such as movement, animations etc. (as well as the ally)
console.log("Player Loaded");

//takes the inputs and event listenrs from main and allows to move the player
function playerMVMT()
{
    if (gamerInput.action === "Left")
    {
        xposition -= 5;
        walking = 1;
    }
    if (gamerInput.action === "Right")
    {
        xposition += 5;
        walking = 2;
    }
    if (gamerInput.action === "None")
    {
        walking = 0;
        petdir = 0;
    }
}

//takes the inputs and event listenrs from main and allows to move the player
function playerMVMTTopDown()
{
    //only allows the player tio jump when they havent jumped
    if (gamerInput.action === "Up")
    {
        yposition -= 6;
        direction =0;
    }
    if (gamerInput.action === "Down")
    {
        yposition += 6;
        direction = 1;
    }
    if (gamerInput.action === "Left")
    {
        xposition -= 6;
        direction = 2;
    }
    if (gamerInput.action === "Right")
    {
        xposition += 6;
        direction = 3;
    }
    if (gamerInput.action === "None")
    {
        walking = 0;
    }
}

//the pet follows function, down close to the legs and nearby
function alliesFollow()
{
    //ally
    if (petY < yposition + 140)
    {
        petY += 5;
    }
    if (petY > yposition + 140)
    {
        petY -= 5;
    }
    if (petX < xposition - 30)
    {
        petX += 5;
        petdir = 2;
    }
    if (petX > xposition + 50)
    {
        petX -= 5;
        petdir = 1;
    }
}


//animates the player
function playerAnimate()
{
    playercurrent = new Date().getTime();
    if (playercurrent - playerinitial >= 200) {
        playercurrentFrame = (playercurrentFrame + 1) % playerframes;
        playerinitial = playercurrent;
    }

}

//animates the pet
function petAnimate()
{
    petcurrent = new Date().getTime();
    if (petcurrent - petinitial >= 200) {
        petcurrentFrame = (petcurrentFrame + 1) % petframes;
        petinitial = petcurrent;
    }

}

function playerBounds()
{
    //only move camera in the side scenes, not top downs
    if (scene == "home" || scene == "town")
    {
        //camera follows player
        if (xposition > 900 ) 
        {
            camerax -= 5;
            petdir = 2;
        }
        if (xposition < 200 ) 
        {
            camerax += 5;
            petdir = 1;
        }

        //doesn't allow player to become out of bounds (moves the screen, not the player)
        if (xposition >= 900 ) 
        {
            xposition = 900;
        }
        if (xposition <= 200 ) 
        {
            xposition = 200;
        }
    }   
    else
    {
        //doesn't allow player to become out of bounds (moves the screen, not the player)
        if (xposition >= 920 ) 
        {
            xposition = 920;
        }
        if (xposition <= 230 ) 
        {
            xposition = 230;
        }
        if (yposition >= 540 ) 
        {
            yposition = 540;
        }
        if (yposition <= 10 ) 
        {
            yposition = 10;
        }
    }

    //checks the position of berry and player, size of player and berry are the numbers
    if (xposition <= (berryX + 32) && (xposition + 50) >= berryX  && yposition <= (berryY + 32) && (yposition + 50) >= berryY)
    {
        berries += 1;
        berryX = (Math.floor(Math.random() * 690) + 230);
        berryY = (Math.floor(Math.random() * 530) + 10);
    }


}


function enemyChasing()
{
    //enemy 1
    if (e1y < yposition)
    {
        e1y += 1;
    }
    if (e1y > yposition)
    {
        e1y -= 1;
    }
    if (e1x < xposition)
    {
        e1x += 1;
    }
    if (e1x > xposition)
    {
        e1x -= 1;
    }


    //enemy 2 up and down
    if (e2y >= 550)
    {
        e2move = -4;
    }
    if (e2y <= 0)
    {
        e2move = 4;
    }
    e2y += e2move;

    //enemy 3 side to side
    if (e3x >= 900)
    {
        e3move = -3;
    }
    if (e3x <= 230)
    {
        e3move = 3;
    }
    e3x += e3move;

    //enemy 4 in a square
    //move to the right
    if (e4y > 450)
    {
        e4xmove = 2;
        e4ymove = 0;
        console.log("moving right");
    }
    //move up
    if (e4x > 850)
    {
        e4xmove = 0;
        e4ymove = -2;
        console.log("moving up");
    } 
    //move left
    if (e4y < 50)
    {
        e4xmove = -2;
        e4ymove = 0;
        console.log("moving left");
    }
    //move down
    if (e4x < 300)
    {
        e4xmove = 0;
        e4ymove = 2;
        console.log("moving down");
    }

    //bottom left corner reset, cause the slime was being a little s
    if (e4y > 450 && e4x < 300)
    {
        e4x = 300;
        e4y = 450;
    }

    e4y += e4ymove;
    e4x += e4xmove;

}


function step() 
{
    if (berries >= 1)
    {
        counter++;
        if (counter > 15) 
        {
            berries -= 1;
            counter = 0;
        }
    }
}

function enemyCollides()
{
    if (xposition <= (e1x + 50) && (xposition + 50) >= e1x  && yposition <= (e1y + 50) && (yposition + 50) >= e1y) 
    {   
        step();
    }
    if (xposition <= (e2x + 50) && (xposition + 50) >= e2x  && yposition <= (e2y + 50) && (yposition + 50) >= e2y) 
    {   
        step();
    }
    if (xposition <= (e3x + 50) && (xposition + 50) >= e3x  && yposition <= (e3y + 50) && (yposition + 50) >= e3y) 
    {   
        step();
    }
    if (xposition <= (e4x + 50) && (xposition + 50) >= e4x  && yposition <= (e4y + 50) && (yposition + 50) >= e4y) 
    {   
        step();
    }
}