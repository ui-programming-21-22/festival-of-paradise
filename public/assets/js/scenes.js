//background scrolling
console.log ("scenes loaded");

//marty, make functions for all stats that change for scenes, dont be stupid <3 
//i did past marty <3

function draw()
{
    context.clearRect(0, 0, canvas.width, canvas.height); //always clear the shit yo

    if (scene == "home") //home scene
    {
        context.drawImage(home, camerax, 0, 4800, canvas.height);
        context.drawImage(petIMG,  (90 * petcurrentFrame), (petdir * 128), 90, 128, petX, petY, 90, 128);
        context.drawImage(playerIMG, (128 * playercurrentFrame), (270 * walking), 128, 270, xposition, yposition + ymod, 128, psize);
        context.drawImage(dialogue, diax, 20, 600, 160);
    }

    if (scene == "town") //town scene
    {
        context.drawImage(town, camerax, 0, 12000, canvas.height);

        //3 if statements for one npc, this will be painful, but this worked so not too bad
        //only draw a sprite when player is in the area
        if (camerax <= -5400 && camerax >=-6700) //did about 1400 pixels for the width of the npc sprite, better safe than sorry
        {    
            //only move the sprite when the player is on the border (aka moving the screen check devlog 3 for the funny thing that happened)
            if(xposition <= 200 || xposition >= 900)
            {
                //move the baker pos in opposite direction to the player
                if (gamerInput.action === "Left")
                {
                    bakerPos += 5;
                }
                if (gamerInput.action === "Right")
                {
                    bakerPos -= 5;
                }   
            }   
            //same animation speed as player, so just reused it, use the baker pos calculated above.
            context.drawImage(bakerIMG, (128 * playercurrentFrame), 0, 128, 270, bakerPos, 255, 128, 270);
        }

        if (camerax <= -300 && camerax >= -1800) //did about 1400 pixels for the width of the npc sprite, better safe than sorry
        {    
            //only move the sprite when the player is on the border (aka moving the screen check devlog 3 for the funny thing that happened)
            if(xposition <= 200 || xposition >= 900)
            {
                //move the baker pos in opposite direction to the player
                if (gamerInput.action === "Left")
                {
                    farmerPos += 5;
                }
                if (gamerInput.action === "Right")
                {
                    farmerPos -= 5;
                }   
            }   
            context.drawImage(farmerIMG, (128 * playercurrentFrame), 0, 128, 270, farmerPos, 255, 128, 270);
        }

        if (camerax <= -7200 && camerax >=-8500) //did about 1400 pixels for the width of the npc sprite, better safe than sorry
        {    
            //only move the sprite when the player is on the border (aka moving the screen check devlog 3 for the funny thing that happened)
            if(xposition <= 200 || xposition >= 900)
            {
                //move the baker pos in opposite direction to the player
                if (gamerInput.action === "Left")
                {
                    wmakerPos += 5;
                }
                if (gamerInput.action === "Right")
                {
                    wmakerPos -= 5;
                }   
            }   
            //same animation speed as player, so just reused it, use the baker pos calculated above.
            context.drawImage(wmakerIMG, (128 * playercurrentFrame), 0, 128, 270, wmakerPos, 275, 128, 270);
        }
        if (camerax <= -7800 && camerax >=-9100) //did about 1400 pixels for the width of the npc sprite, better safe than sorry
        {    
            //only move the sprite when the player is on the border (aka moving the screen check devlog 3 for the funny thing that happened)
            if(xposition <= 200 || xposition >= 900)
            {
                //move the baker pos in opposite direction to the player
                if (gamerInput.action === "Left")
                {
                    bardPos += 5;
                }
                if (gamerInput.action === "Right")
                {
                    bardPos -= 5;
                }   
            }   
            //same animation speed as player, so just reused it, use the baker pos calculated above.
            context.drawImage(bardIMG, (128 * playercurrentFrame), 0, 128, 270, bardPos, 260, 128, 270);
        }

        if (camerax <= -9500 && camerax >=-10800) //did about 1400 pixels for the width of the npc sprite, better safe than sorry
        {    
            //only move the sprite when the player is on the border (aka moving the screen check devlog 3 for the funny thing that happened)
            if(xposition <= 200 || xposition >= 900)
            {
                //move the baker pos in opposite direction to the player
                if (gamerInput.action === "Left")
                {
                    curPos += 5;
                }
                if (gamerInput.action === "Right")
                {
                    curPos -= 5;
                }   
            }   
            //same animation speed as player, so just reused it, use the baker pos calculated above.
            context.drawImage(curatorIMG, (128 * playercurrentFrame), 0, 128, 270, curPos, 260, 128, 270);
        }
        context.drawImage(petIMG,  (90 * petcurrentFrame), (petdir * 128), 90, 128, petX, petY, 90, 128);
        context.drawImage(playerIMG, (128 * playercurrentFrame), (270 * walking), 128, 270, xposition, yposition + ymod, 128, psize);

        context.drawImage(dialogue, diax, 20, 600, 160);
    }

    if (scene == "forest")
    {
        context.drawImage(forest, 0, 0, canvas.width, canvas.height);
        context.drawImage(playerIMG, (53 * playercurrentFrame), (53 * direction), 53, 53, xposition, yposition, 50, 50);
        context.drawImage(e1, (50 * e1currentFrame), 0, 50, 50, e1x, e1y, 50, 50);
        context.drawImage(e2, (50 * e2currentFrame), 0, 50, 50, e2x, e2y, 50, 50);
        context.drawImage(e3, (50 * e3currentFrame), 0, 50, 50, e3x, e3y, 50, 50);
        context.drawImage(e4, (50 * e1currentFrame), 0, 50, 50, e4x, e4y, 50, 50);
        context.drawImage(berry, berryX, berryY);
        if (berries >= 15)
        {
            context.fillStyle = "#5cff1e";
            context.fillText('Task Complete!', 10, 110);

        }
        if (berries >= 30)
        {
            context.fillStyle = "#5cff1e";
            context.fillText('Please Leave Already!', 10, 140);

        }
        if (berries >= 50)
        {
            context.fillStyle = "#5cff1e";
            context.fillText('There is nothing here...', 10, 170);

        }
        if (berries >= 70)
        {
            context.fillStyle = "#5cff1e";
            context.fillText('You must be bored..', 10, 200);

        }
        if (berries >= 100)
        {
            context.fillStyle = "#f21616";
            context.fillText('FINE FINE! You win! You got the secret ending, now leave!', 300, 50);
            endingIMG.src = "assets/media/endings/ending10.png";

        }
        else
        {
            context.fillStyle = "#fff";
        }
        context.font = "22px Arial";
        context.fillText('Berries Gathered: ' + berries, 10, 80);
    }
}


function changeScene() 
{ 
    //this is just for leaving the house but it can be easily resued
    //bounding if the player game from left then right
    //allows leeway eitherway

    //house to town
if (scene == "home")
{
    //coming from the left
    if (camerax <= -1100 && camerax >= -1750 && xposition >= 600 && xposition <= 900)
    {
        context.drawImage(arrow, (xposition + 32), 200, 50,50 );

        if (gamerInput.action === "Space")
        {
            setTimeout(function() 
            { 
                scene = "town";
                townvars();
            }, 300);
        }
    }

    //coming from the left
    if (camerax <= 0 && camerax >= -300 && xposition >= 200 && xposition <= 400)
    {
        context.drawImage(arrow, (xposition + 32), 200, 50,50 );

        if (gamerInput.action === "Space")
        {
            setTimeout(function() 
            { 
                game = "over";
            }, 300);
        }
    }

    //coming from the right 
    if (camerax <= -1550 && camerax >= -2100 && xposition >= 200 && xposition <= 600)
        {
            context.drawImage(arrow,(xposition + 32), 200, 50,50 );

            if (gamerInput.action === "Space")
            {
                setTimeout(function() 
                { 
                    scene = "town";
                    townvars();
                }, 300);
            }
        }   
    }
    //town to house
    if (scene == "town")
    {
        //coming from left
        if (camerax <= -4300 && camerax >= -4950 && xposition >= 600 && xposition <= 900)
        {
            context.drawImage(arrow, (xposition + 32), 200, 50,50 );

            if (gamerInput.action === "Space")
            {
                setTimeout(function() 
                { 
                    scene = "home";
                    homevars();
                }, 300);
            }
        }
        //coming from the right 
        if (camerax <= -4750 && camerax >= -5300 && xposition >= 200 && xposition <= 600)
        {
            context.drawImage(arrow,(xposition + 32), 200, 50,50 );

            if (gamerInput.action === "Space")
            { 
                setTimeout(function() 
                { 
                    scene = "home";  
                    homevars();
                }, 300);
            }
        }  
    }

    //from town to forest
    if (scene == "town")
    {
        if (camerax <= 0 && camerax >= -600)
        {
            context.drawImage(arrow, (xposition + 32), 200, 50,50 );

            if (gamerInput.action === "Space")
            {
                setTimeout(function() 
                { 
                    scene = "forest";
                    forestvars();
                }, 300);
            }
        }
    }

    //from forest to town
    if (scene == "forest")
    {
        if (yposition >= 520 && xposition >= 920)
        {
            context.drawImage(arrow, (xposition + 25), 900, 20, 20 );

            if (gamerInput.action === "Space")
            {
                setTimeout(function() 
                { 
                    scene = "town";
                    townvars2();
                }, 300);
            }
        }
    }
}

//checker for when player is near the NPC (needed for each npc)
function bakerTalk()
{
    if (camerax <= -5400 && camerax >=-6700)//the same area the baker sprite is drawn
    {    
        //boundary checking
        if(xposition < (bakerPos + 64) && (xposition + 64) > bakerPos)
        {
            context.drawImage(arrow,(xposition + 32), 200, 50,50 );
            //say baker dialogue start
            if (gamerInput.action === "Space")
            {
                bakerQuestLine();
            }
 
        }   
    }
}

function farmerTalk()
{  
    if (camerax <= -300 && camerax >= -1800) //did about 1400 pixels for the width of the npc sprite, better safe than sorry
    {    
        //boundary checking
        if(xposition < (farmerPos + 64) && (xposition + 64) > farmerPos)
        {
            context.drawImage(arrow,(xposition + 32), 200, 50,50 );
            if (bakerQ <= 1)
            {
                if (gamerInput.action === "Space")
                {
                    dialogue.src = "assets/media/dialogue/farmerwarn.png";
                    diax= 300;
                    setTimeout(function()
                    {
                        diax = -1000;
                    }, 2000);   
                }
            }
            if (bakerQ >= 2 && bakerQ <= 4)
            {
                if (gamerInput.action === "Space")
                {
                    bakerQ = 3;
                    endingIMG.src = "assets/media/endings/ending2.png";
                    bakerQuestLine();
                }  
            }
        }   
    }

}


//example of how dialogue will work
function noMilk()
{
    if(camerax == -3600)
    {    
        if(xposition >= 850)
        {
            context.drawImage(arrow,(xposition + 32), 200, 50,50 );

            if (gamerInput.action === "Space")
            {
                diax= 300;

                setTimeout(function()
                {
                    diax = -1000;
                }, 5000);   
                
            }
        }
    } 
}



//all the camera movement for scenes
//note it is the size of background - canvas (can be done here or manually i do manually as im not chaning canvas width any time)
function cameraMovement()
{
    //when the camera reaches the end, stop moving the camera
    if (scene == "home")
    {
        if (camerax >= 0)
        {
            camerax = 0;
        }
        else if (camerax <= -3600)
        {
        camerax = -3600;
        }
    }

    if (scene == "town")
    {
        if (camerax >= 0)
        {
            camerax = 0;
        }
        else if (camerax <= -10800)
        {
            camerax = -10800;
        }
    }
}


//all the variables for scene changes
function homevars()
{
    camerax = -1700;
    xposition = 500;
    petX = 500;
    yposition = 320;
    petY = 460;
    playerIMG.src = "assets/media/player/playeranimate.png";
}

//from center (npcs need their default values)
function townvars()
{
    camerax = -5000;
    xposition = 500;
    petX = 500;
    yposition = 265;
    petY = 410;
    bakerPos = 1200;
    farmerPos = -200;
    playerIMG.src = "assets/media/player/playeranimate.png";
}

//from side, left npcs need values like right
function townvars2()
{
    camerax = 0;
    xposition = 200;
    petX = 200;
    yposition = 265;
    petY = 410;
    bakerPos = 1200;
    farmerPos = 1200;
    playerIMG.src = "assets/media/player/playeranimate.png";
}

function forestvars()
{
    xposition = 930;
    yposition = 520;
    direction = 2;
    playerIMG.src = "assets/media/player/playerTD.png";
}
