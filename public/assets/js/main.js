//global variables, so it doesn't fuck with other code
console.log("Variables Loaded");

////doesnt allow for arrow keys and space bar to move the screen
window.addEventListener("keydown", function(e) {
    if(["Space","ArrowUp","ArrowDown","ArrowLeft","ArrowRight"].indexOf(e.code) > -1) {
        e.preventDefault();
    }
}, false);

//web storage info
let username = localStorage.getItem('username'); 
let age = localStorage.getItem('age');
let animal = localStorage.getItem('animal');

//gamer input
let gamerInput = new GamerInput("None"); 

let scene = "home"; //this controlls where in the game we are
let game = "ayo" // doesnt matter what its called, just when over, calls the ending scripts

//get canvas and set it up uwu
const canvas = document.getElementById("canvas");
const context = canvas.getContext("2d");

//game variables
let start = 0; //starts the game or pauses it <3
let walking = 0; //decides the walking direction of player
let direction = 0; //direction in teh top down sections of the game
let petdir = 0; //decides when walk when idle for the pet (cause its one sprite sheet for each)

let diax = -1000; //dialogue x
let berries = 0; //forest minigame;
let counter = 0;//for slowing shtuff down

//quest variables
let action = 1; //0 as no quest dialogue, then cycles thru the dialogue and ends with 0 readay for next NPC
//0= not started, 1= started (talked to NPC), 2= in progress (gotten a thing etc), 3=done
let bakerQ = 0;


//starting position, can be adjust later uwu
let xposition = 500;
let yposition = 320;
let ymod = 0; //mdoifer for the x, adds when the sprite is smaller (kid edition)
let petX = 400;
let petY = 470;
let berryX = (Math.floor(Math.random() * 690) + 230);
let berryY = (Math.floor(Math.random() * 530) + 10);

//enemy positions for the forest
let e1x = 100;
let e2x = 600;
let e3x = 1000;
let e4x = 350;                //we want them to spawn left, top, right ^ < like that
let e1y = 300;
let e2y = 50;
let e3y = 300;
let e4y = 450;

let e2move = 4;
let e3move = 3;
let e4ymove = 0;
let e4xmove = 2;

//if a character is on the left of house, give -200, if on right, give 1200
let bakerPos = 1200;
let farmerPos = -200;
let wmakerPos = 1200;
let bardPos = 1200;
let curPos = 1200;

let camerax = -200; //starting camera state

//change player size if child
let psize = 270;

//==============================================================================animations
//each character with different animation speed needs their own animations
//however ill used the player animation speed for npcs too
//animation variables
let playerframes = 6;
let petframes = 4;
let e1frames = 9; //slime 4 uses the same speed as slime 1
let e2frames = 9;
let e3frames = 9;

//each indivdual animation needs its own current frame
let playercurrentFrame = 0; //used for animations
let petcurrentFrame = 0;
let e1currentFrame = 0;
let e2currentFrame = 0;
let e3currentFrame = 0;

//time
let playerinitial = new Date().getTime();
let petinitial = new Date().getTime();
let e1initial = new Date().getTime();
let e2initial = new Date().getTime();
let e3initial = new Date().getTime();
let playercurrent; 
let petcurrent;
let e1current;
let e2current;
let e3current;

//============================================================================images

//images
let instructions = new Image();
let inventoryIMG = new Image();
let endingIMG = new Image();
let eggIMG = new Image();

let playerIMG = new Image();
let petIMG = new Image();
let e1 = new Image();
let e2 = new Image();
let e3 = new Image();
let e4 = new Image();
let home = new Image();
let town = new Image();
let forest = new Image();

let bakerIMG = new Image();
let farmerIMG = new Image();
let wmakerIMG = new Image();
let bardIMG = new Image();
let curatorIMG = new Image();

let arrow = new Image();
let berry = new Image();
let dialogue = new Image();

//sprites - sources
instructions.src = "assets/media/misc/instructions.png";
inventoryIMG.src = "assets/media/misc/inventoryempty.png";
eggIMG.src = "assets/media/misc/eggs.png";

playerIMG.src = "assets/media/player/playeranimate.png";
e1.src = "assets/media/enemies/slImeanimate.png";
e2.src = "assets/media/enemies/slImeanimate2.png";
e3.src = "assets/media/enemies/slImeanimate3.png";
e4.src = "assets/media/enemies/slimeanimate4.png";

home.src = "assets/media/bgs/house.png";
town.src = "assets/media/bgs/town.png";
forest.src = "assets/media/bgs/forest.png";

bakerIMG.src = "assets/media/npcs/bakeridle.png";
farmerIMG.src = "assets/media/npcs/fameridle.png";
wmakerIMG.src = "assets/media/npcs/winemakeridle.png";
bardIMG.src = "assets/media/npcs/bardidle.png";
curatorIMG.src = "assets/media/npcs/curatoridle.png";

arrow.src = "assets/media/misc/arrow.png";
berry.src = "assets/media/misc/berry.png";
dialogue.src = "assets/media/dialogue/nomilk.png";

//change animal sprite depending on what it is
if (animal == "cat")
{
    petIMG.src = "assets/media/player/catanimate.png";
    endingIMG.src = "assets/media/endings/ending1cat.png";
}
else
{
    petIMG.src = "assets/media/player/doganimate.png";
    endingIMG.src = "assets/media/endings/ending1dog.png";
}
//hi colm, this is for you :)
if (username == "colm" || username == "Colm" || username == "COLM")
{
    petIMG.src = "assets/media/player/duckanimate.png";
    endingIMG.src = "assets/media/endings/ending1duck.png";
}

//check age and make sprite small or big
if(age < 18)
{
    psize = 250; //makes player smaller
    ymod = 20; //moves the player down a little
}

//====================================================================================functions
//game loop, only starts updating and drawing when the player presses start
//then allows the game to be paused (this is my standard, always needed)
function gameLoop()
{
    //these functions gotta be outside the if, otherwise you cant start at all
    startGame();
    pauseGame();
    context.drawImage(instructions,0,0);
    if (start == 1)
    { //only start playing when the player is ready
        draw();
        update();
        inventoryOpen();
    }

    requestAnimationFrame(gameLoop);
}

//inputs from player
function GamerInput(input)
{
    //input is passed from input functions, left, right etc.
    this.action = input;
}

//=========================================================================mobile input
//creates the nipple on the canvas, it is invisible, to allow
//the player to just use anywhere, left or right side
var joystick = nipplejs.create
({
    color: 'DarkMagenta',
    zone: document.getElementById('canvas'),
    mode: 'dynamic',
});

//nipple.on('start move end dir plain', function (evt) {
joystick.on('dir:up', function (evt, data) 
    {
        gamerInput = new GamerInput("Up");
    });
joystick.on('dir:down', function (evt, data) 
    {
        gamerInput = new GamerInput("Down");
    });
joystick.on('dir:left', function (evt, data) 
    {
        gamerInput = new GamerInput("Left");
    });
joystick.on('dir:right', function (evt, data) 
    {
        gamerInput = new GamerInput("Right");
    });
joystick.on('end', function (evt, data) 
    {
        gamerInput = new GamerInput("None");
    });


//function for when the button is pressed
//these actually work on mobile ✨
function interact()
{
    gamerInput = new GamerInput("Space");
    console.log("Interact!");
}

function inventory()
{
    gamerInput = new GamerInput("Inventory");
}

function stopinteract()
{
    gamerInput = new GamerInput("None");
}


function pauseButton()
{
    gamerInput = new GamerInput("Pause");    
}

function startButton()
{
    gamerInput = new GamerInput("Start");    
}

//=================================================================================desktop input
//all the inputs that are going to be used in the game
function input(event)
{
    //listens for any keydown events, aka the input from the user.
    if (event.type === "keydown")
    {
        //accounts for the 4 directional keys, and a default of none.
        switch (event.keyCode) 
        {
            case 37: // Left Arrow
                gamerInput = new GamerInput("Left");
                break;
            case 65: // A
                gamerInput = new GamerInput("Left");
                break;

            case 38: // Up Arrow
                gamerInput = new GamerInput("Up");
                break; 
            case 87: // W
                gamerInput = new GamerInput("Up");
                break;

            case 39: // Right Arrow
                gamerInput = new GamerInput("Right");
                break;
            case 68: // D
                gamerInput = new GamerInput("Right");
                break;

            case 40: // Down Arrow
                gamerInput = new GamerInput("Down");
                break;
            case 83: // S
                gamerInput = new GamerInput("Down");
                break;

            case 13: //enter
                gamerInput = new GamerInput("Start");        
                break;
            
            case 80: //p
                gamerInput = new GamerInput("Pause");        
                break;

            case 32: //space
                gamerInput = new GamerInput("Space");        
                break;

            case 69: // e
                gamerInput = new GamerInput("Inventory");
                break;
            }

        }
        //if no key is pressed, do nothing.
        else
        {
            gamerInput = new GamerInput("None");
        }
}


//============================================================================game functions

//starts the game
function startGame()
{
    if (gamerInput.action === "Start")
    {
        start = 1;
    }
}
//pauses the game
function pauseGame()
{
    if (gamerInput.action === "Pause")
    {
        start = 0;
        context.clearRect(0, 0, canvas.width, canvas.height);
    }
}

function inventoryOpen()
{
    if (gamerInput.action === "Inventory")
    {
        context.drawImage(inventoryIMG, 0,0);
        if (bakerQ >= 3 && bakerQ <= 6)
        {
            context.drawImage(eggIMG, 0,0);
        }
    }

}

//relies on the game being started
//updates other functions
function update()
{
    alliesFollow();
    playerAnimate();
    petAnimate();
    playerBounds();
    cameraMovement();
    changeScene();

    if (scene == "home")
    {
        playerMVMT();
        noMilk();
        //functions only aplicable to home
    }
    if (scene == "town")
    {
        playerMVMT();
        if (bakerQ != 6)
        {
            bakerTalk();
            farmerTalk();
            bakerQuestStart();
        }
    }
    if (scene == "forest")
    {
        playerMVMTTopDown();
        enemyChasing();
        enemyAnimations();
        enemyCollides();
        //functions only aplicable in forest
    }
    if (scene == "museum")
    {
        playerMVMTTopDown();
        //functions only applicable to museum
    }
    if (game == "over")
    {
        endingScript();
    }
}


//event listeners
window.requestAnimationFrame(gameLoop);
window.addEventListener('keydown',input);
window.addEventListener('keyup',input);

var el = document.getElementById("interact");
el.addEventListener("touchstart", interact, true);
el.addEventListener("touchend", stopinteract, true);

var el = document.getElementById("inventory");
el.addEventListener("touchstart", inventory, true);
el.addEventListener("touchend", stopinteract, true);

//============================================================================Saved Data and Scene loader


//fuck loading and saving, play it one go, not my problem rn\

function enemyAnimations()
{
    //enemy 1 is slowest
    e1current = new Date().getTime();
    if (e1current - e1initial >= 250) {
        e1currentFrame = (e1currentFrame + 1) % e1frames;
        e1initial = e1current;
    }
    //enemy 2 is the fastest
    e2current = new Date().getTime();
    if (e2current - e2initial >= 150) {
        e2currentFrame = (e2currentFrame + 1) % e2frames;
        e2initial = e2current;
    }
    //enemy 3 is medium
    e3current = new Date().getTime();
    if (e3current - e3initial >= 200) {
        e3currentFrame = (e3currentFrame + 1) % e3frames;
        e3initial = e3current;
    }
}

//last scene checker (to speed up debuggin but also loading game)
    if (scene == "home")
    {
        homevars();
    }

    if (scene == "town" )
    {
        townvars();
    }

    if (scene == "forest")
    {
        forestvars();
    }

function endingScript()
{

    context.drawImage(endingIMG, 0, 0);
}

